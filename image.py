import cv2
from matplotlib import pyplot as plot

#sta por defecto es leída en BGR
imagen = cv2.imread('image.jpg', -1) 


#La imagen será leída en escala de grises
imagen2 = cv2.imread('image.jpg',0)

#usando matplotlib
imagen3 = cv2.imread('image.jpg')
imagen3 = cv2.cvtColor(imagen, cv2.COLOR_BGR2RGB)

#-----resize
#print original size of the image
print('Original Dimensions : ',imagen2.shape)

scale_percent = 10 # percent of original size

width = int(imagen2.shape[1] * scale_percent / 100)
height = int(imagen2.shape[0] * scale_percent / 100)
dim = (width, height)

resized = cv2.resize(imagen2, dim, interpolation = cv2.INTER_AREA) #resize image

print('Resized Dimensions : ',resized.shape) #print new size of the image

#visualizar la imagen
cv2.imshow('original',imagen)
cv2.waitKey(0)
cv2.destroyAllWindows()

#viw the rezised image
cv2.imshow("Resized image", resized)
cv2.waitKey(0)
cv2.destroyAllWindows()

#sow with matplotlib
plot.imshow(imagen3)
plot.show()
