import cv2
from matplotlib import pyplot as plot

img1 = cv2.imread('image.jpg', 0)
img1= cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

img2 = cv2.imread('image.jpg')
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)

img3 = cv2.imread('image.jpg')

img4 =cv2.imread('image.jpg')
hist = cv2.calcHist([img4], 0, None, [256], [0, 256])



fig, axs = plot.subplots(2, 2)
axs[0, 0].imshow(img1)
axs[0, 0].set_title('Axis [0, 0]')
axs[0, 1].imshow(img2)
axs[0, 1].set_title('Axis [0, 1]')
axs[1, 0].imshow(img3)
axs[1, 0].set_title('Axis [1, 0]')
axs[1, 1].hist(img4.ravel(), 256, [0, 256])
axs[1, 1].set_title('Axis [1, 1]')

for ax in axs.flat:
    ax.set(xlabel='x', ylabel='y')

for ax in axs.flat:
    ax.label_outer()


plot.show()